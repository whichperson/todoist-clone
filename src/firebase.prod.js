import firebase from 'firebase/app';
import 'firebase/firestore';

const fireBaseConfig = firebase.initializeApp({
  apiKey: '',
  authDomain: '',
  databaseURL: '',
  projectId: '',
  storageBucket: '',
  messagingSenderId: '',
  appId: ''
});

export { fireBaseConfig as firebase };