# [Todoist Clone](https://todoist.com/) ✅

This is a clone of the web app Todoist written in React. You can download the source code
and run it in your local environment. 

## Why make this? 
I use Todoist daily to organize my life and keep track of my progress. So, to practice my React skills, I decided to clone it. 


## Features
Because this was a one-day-build challenge, I did not manage to implement Todoist's complete functionality. 
I chose to focus on the most important features.
* Projects: 
    * Create up to 5 projects
    * Click trash icon to delete
* Tasks:
    * Quick add task option: Click the plus sign to quickly add a task
    * Add unlimited tasks
    * Select due date 
    * Select project label
    * Click circle to mark as done
* Darkmode option: Click the lightbulb icon to toggle darkmode on/off. 

## Tools 🔨
* [Firebase](https://firebase.google.com/)
* [Lighthouse](https://developers.google.com/web/tools/lighthouse)
* React Testing Library

Written in:
* React (JavaScript framework)
* HTML5/Sass 

## Screenshots
![](https://i.imgur.com/IQ3bmRL.png)
